import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'

import Login from '../components/Login';
import { validate } from '../actions/validate';

const mapStateToProps = (state) => ({
  isAuth: state.login.isAuth,
  loading: state.login.loading,
  error: state.login.error
});

const mapDispatchToProps = (dispatch) => ({
  handleClick: (domain, username, password) => dispatch(validate(domain, username, password)),
  navigateTo: (routeName: string) => {
    const actionToDispatch = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName })]
    })
    dispatch(actionToDispatch)
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
