import { connect } from 'react-redux';

import Access from '../components/Access';
import { getAccess } from '../actions/access';

const mapStateToProps = (state) => ({
  list: state.access.list,
  loading: state.access.loading,
  error: state.access.error
});

const mapDispatchToProps = (dispatch) => ({
  handleOnLoad: () => dispatch(getAccess())
});

export default connect(mapStateToProps, mapDispatchToProps)(Access);
