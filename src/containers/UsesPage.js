import { connect } from 'react-redux';

import Uses from '../components/Uses';
import { getReservations } from '../actions/reservations';
import { getServices } from '../actions/services';

const mapStateToProps = (state) => ({
  list: state.services.list,
  loading: state.services.loading,
  error: state.services.error
});

const mapDispatchToProps = (dispatch) => ({
  handleOnLoad: () => {
    dispatch(getServices())
    dispatch(getReservations())
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Uses);
