import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'

import Token from '../components/Token';
import { getQRCode } from '../actions/qr';

const mapStateToProps = (state) => ({
  qr: state.qr
});

const mapDispatchToProps = (dispatch) => ({
  handleLoadQR: () => dispatch(getQRCode())
});

export default connect(mapStateToProps, mapDispatchToProps)(Token);
