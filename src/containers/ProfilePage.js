import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'

import Profile from '../components/Profile';
import { getProfile, logout } from '../actions/profile';

const mapStateToProps = (state) => ({
  profile: state.profile.profile,
  loading: state.profile.loading,
  error: state.profile.error
});

const mapDispatchToProps = (dispatch) => ({
  handleOnLoad: () => dispatch(getProfile()),
  logout: () => {
    const actionToDispatch = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName:'Login' })]
    })
    dispatch(actionToDispatch)
    dispatch(logout())
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
