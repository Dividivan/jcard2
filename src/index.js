import React, { Component } from 'react'
import { Provider, connect } from 'react-redux'
import { addNavigationHelpers } from 'react-navigation'
import { Text, AppState, AsyncStorage, View } from "react-native"

import Navigator from './routes'

import getStore from './store'

const navReducer = (state, action) => {
  const newState = Navigator.router.getStateForAction(action, state)
  return newState || state
}

class App extends Component {
  render(){
    return (
      <Navigator
        navigation={addNavigationHelpers({
            dispatch: this.props.dispatch,
            state: this.props.nav
        })}
      />
    )
  }
}

const store = getStore(navReducer);
const AppIndex = connect( state => ({ nav: state.nav }) )(App)

export default class Index extends Component {
  constructor(props){
    super(props);
    this.state = {
      isStoreLoading: false,
      store: store
    }
  }

  componentWillMount() {
    var self = this;
    AppState.addEventListener('change', this._handleAppStateChange.bind(this));
    this.setState({isStoreLoading: true});
    AsyncStorage.getItem('completeStore').then((value)=>{
      if(value && value.length){
        let initialStore = JSON.parse(value)
        initialStore.login.loading = false
        self.setState({store: getStore(navReducer, initialStore)})
      }else{
        self.setState({store: store});
      }
      self.setState({isStoreLoading: false});
    }).catch((error)=>{
      self.setState({store: store});
      self.setState({isStoreLoading: false});
    })
  }
  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange.bind(this));
  }
  _handleAppStateChange(currentAppState) {
    let storingValue = JSON.stringify(this.state.store.getState())
    AsyncStorage.setItem('completeStore', storingValue);
  }

  render(){
    if(this.state.isStoreLoading){
      return <Text>Loading Store ...</Text>
    }else{
      return (
        <Provider store={this.state.store}>
          <AppIndex />
        </Provider>
      )
    }
  }
}
