import * as ACTIONS_TYPE from '../constants/actionsType'

export const getReservationsAction = () => ({
  type: ACTIONS_TYPE.GET_RESERVATIONS,
  payload: {}
})

export const getReservationsSuccessAction = (reservations) => ({
  type: ACTIONS_TYPE.GET_RESERVATIONS_SUCCESS,
  payload: {
    reservations
  }
})

export const getReservationsErrorAction = (error) => ({
  type: ACTIONS_TYPE.GET_RESERVATIONS_FAIL,
  payload: {
    error
  }
})

export function getReservations() {
  return (dispatch, getState) => {
    const token = getState().login.token
    const domain = getState().login.domain
    const body = {
      token
    }
    const formBody = Object.keys(body).map(key=>encodeURIComponent(key)+'='+encodeURIComponent(body[key])) .join('&')

    dispatch(getReservationsAction())

    return fetch(`https://${domain}/api/reservastions`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic amNhcmRfYXBpX3VzZXI6dTZNMUtxVnFnamV0M0NMY2FNeTdhZXlLTkFsd1FDT2o='
      },
      body: formBody
    })
      .then((response) => response.json())
      .then((responseJson) => {
        const { result: code, content } = responseJson

        switch (code) {
          case 200: {
            dispatch(getReservationsSuccessAction(content))
            break;
          }
          case 400: {
            dispatch(getReservationsErrorAction('Algo fue mal.'))
            break;
          }
          case 401: {
            dispatch(getReservationsErrorAction('No autorizado.'))
          }
          default:
            break;
        }
      })
      .catch((err) => {
        console.log('error', err)
        dispatch(getReservationsErrorAction('Algo falla, asegurse que tiene conexión a internet.'))
      })
  }
}
