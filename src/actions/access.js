import * as ACTIONS_TYPE from '../constants/actionsType'

export const getAccessAction = () => ({
  type: ACTIONS_TYPE.GET_ACCESS,
  payload: {}
})

export const getAccessSuccessAction = (access) => ({
  type: ACTIONS_TYPE.GET_ACCESS_SUCCESS,
  payload: {
    access
  }
})

export const getAccessErrorAction = (error) => ({
  type: ACTIONS_TYPE.GET_ACCESS_FAIL,
  payload: {
    error
  }
})

export function getAccess() {
  return (dispatch, getState) => {
    const token = getState().login.token
    const domain = getState().login.domain
    const body = {
      token
    }
    const formBody = Object.keys(body).map(key=>encodeURIComponent(key)+'='+encodeURIComponent(body[key])) .join('&')

    dispatch(getAccessAction())

    return fetch(`https://${domain}/api/access`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic amNhcmRfYXBpX3VzZXI6dTZNMUtxVnFnamV0M0NMY2FNeTdhZXlLTkFsd1FDT2o='
      },
      body: formBody
    })
      .then((response) => response.json())
      .then((responseJson) => {
        const { result: code, content } = responseJson

        switch (code) {
          case 200: {
            dispatch(getAccessSuccessAction(content))
            break;
          }
          case 400: {
            dispatch(getAccessErrorAction('Algo fue mal.'))
            break;
          }
          case 401: {
            dispatch(getAccessErrorAction('No autorizado.'))
          }
          default:
            break;
        }
      })
      .catch((err) => {
        console.log('error', err)
        dispatch(getAccessErrorAction('Algo falla, asegurse que tiene conexión a internet.'))
      })
  }
}
