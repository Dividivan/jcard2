import * as ACTIONS_TYPE from '../constants/actionsType'

export const validateAction = () => ({
  type: ACTIONS_TYPE.VALIDATE,
  payload: {}
})

export const validateSuccessAction = (domain, token) => ({
  type: ACTIONS_TYPE.VALIDATE_SUCCESS,
  payload: {
    domain,
    token
  }
})

export const validateErrorAction = (error) => ({
  type: ACTIONS_TYPE.VALIDATE_FAIL,
  payload: {
    error
  }
})

export function validate(domain, username, password) {
  return (dispatch, getState) => {
    const body = {
      username,
      password
    }
    const formBody = Object.keys(body).map(key=>encodeURIComponent(key)+'='+encodeURIComponent(body[key])) .join('&')

    dispatch(validateAction())

    return fetch(`https://${domain}/api/validate`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic amNhcmRfYXBpX3VzZXI6dTZNMUtxVnFnamV0M0NMY2FNeTdhZXlLTkFsd1FDT2o='
      },
      body: formBody
    })
      .then((response) => response.json())
      .then((responseJson) => {
        const { result: code, content } = responseJson

        switch (code) {
          case 200: {
            dispatch(validateSuccessAction(domain, content.token))
            break;
          }
          case 400: {
            dispatch(validateErrorAction('Algo fue mal.'))
            break;
          }
          case 401: {
            dispatch(validateErrorAction('Usuario o contraseña incorrectos.'))
            break;
          }
          case 403: {
            dispatch(validateErrorAction('Contraseña obsoleta. Debe loguearse en la aplicación web. https://demo.cloudjcard.com/'))
            break;
          }
          default:
            break;
        }
      })
      .catch(() => {
        dispatch(validateErrorAction('Algo falla, asegurese que el dominio es correcto y tiene conexión a internet.'))
      })
  }
}
