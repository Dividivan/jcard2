import * as ACTIONS_TYPE from '../constants/actionsType'

export const getQRCodeAction = () => ({
  type: ACTIONS_TYPE.GET_ID,
  payload: {}
})

export const getQRCodeSuccessAction = (code) => ({
  type: ACTIONS_TYPE.GET_ID_SUCCESS,
  payload: {
    code
  }
})

export const getQRCodeErrorAction = (error) => ({
  type: ACTIONS_TYPE.GET_ID_FAIL,
  payload: {
    error
  }
})

export function getQRCode() {
  return (dispatch, getState) => {
    const token = getState().login.token
    const domain = getState().login.domain
    const body = {
      token
    }
    const formBody = Object.keys(body).map(key=>encodeURIComponent(key)+'='+encodeURIComponent(body[key])) .join('&')

    dispatch(getQRCodeAction())

    return fetch(`https://${domain}/api/id/png`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic amNhcmRfYXBpX3VzZXI6dTZNMUtxVnFnamV0M0NMY2FNeTdhZXlLTkFsd1FDT2o='
      },
      body: formBody
    })
      .then((response) => response.json())
      .then((responseJson) => {
        const { result: code, content } = responseJson

        switch (code) {
          case 200: {
            dispatch(getQRCodeSuccessAction(content.png))
            break;
          }
          case 400: {
            dispatch(getQRCodeErrorAction('Algo fue mal.'))
            break;
          }
          case 401: {
            dispatch(getQRCodeErrorAction('No autorizado.'))
          }
          default:
            break;
        }
      })
      .catch((err) => {
        console.log('error', err)
        dispatch(getQRCodeErrorAction('Algo falla, asegurse que tiene conexión a internet.'))
      })
  }
}
