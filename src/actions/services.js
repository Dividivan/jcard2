import * as ACTIONS_TYPE from '../constants/actionsType'

export const getServicesAction = () => ({
  type: ACTIONS_TYPE.GET_SERVICES,
  payload: {}
})

export const getServicesSuccessAction = (services) => ({
  type: ACTIONS_TYPE.GET_SERVICES_SUCCESS,
  payload: {
    services
  }
})

export const getServicesErrorAction = (error) => ({
  type: ACTIONS_TYPE.GET_SERVICES_FAIL,
  payload: {
    error
  }
})

export function getServices() {
  return (dispatch, getState) => {
    const token = getState().login.token
    const domain = getState().login.domain
    const body = {
      token
    }
    const formBody = Object.keys(body).map(key=>encodeURIComponent(key)+'='+encodeURIComponent(body[key])) .join('&')

    dispatch(getServicesAction())

    return fetch(`https://${domain}/api/services`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic amNhcmRfYXBpX3VzZXI6dTZNMUtxVnFnamV0M0NMY2FNeTdhZXlLTkFsd1FDT2o='
      },
      body: formBody
    })
      .then((response) => response.json())
      .then((responseJson) => {
        const { result: code, content } = responseJson

        switch (code) {
          case 200: {
            dispatch(getServicesSuccessAction(content))
            break;
          }
          case 400: {
            dispatch(getServicesErrorAction('Algo fue mal.'))
            break;
          }
          case 401: {
            dispatch(getServicesErrorAction('No autorizado.'))
          }
          default:
            break;
        }
      })
      .catch((err) => {
        console.log('error', err)
        dispatch(getServicesErrorAction('Algo falla, asegurse que tiene conexión a internet.'))
      })
  }
}
