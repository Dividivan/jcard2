import * as ACTIONS_TYPE from '../constants/actionsType'

export const logout = () => ({
  type: ACTIONS_TYPE.LOGOUT,
  payload: {}
})

export const getProfileAction = () => ({
  type: ACTIONS_TYPE.GET_PROFILE,
  payload: {}
})

export const getProfileSuccessAction = (profile) => ({
  type: ACTIONS_TYPE.GET_PROFILE_SUCCESS,
  payload: {
    profile
  }
})

export const getProfileErrorAction = (error) => ({
  type: ACTIONS_TYPE.GET_PROFILE_FAIL,
  payload: {
    error
  }
})

export function getProfile() {
  return (dispatch, getState) => {
    const token = getState().login.token
    const domain = getState().login.domain
    const body = {
      token
    }
    const formBody = Object.keys(body).map(key=>encodeURIComponent(key)+'='+encodeURIComponent(body[key])) .join('&')

    dispatch(getProfileAction())

    return fetch(`https://${domain}/api/profile`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic amNhcmRfYXBpX3VzZXI6dTZNMUtxVnFnamV0M0NMY2FNeTdhZXlLTkFsd1FDT2o='
      },
      body: formBody
    })
      .then((response) => response.json())
      .then((responseJson) => {
        const { result: code, content } = responseJson

        switch (code) {
          case 200: {
            dispatch(getProfileSuccessAction(content))
            break;
          }
          case 400: {
            dispatch(getProfileErrorAction('Algo fue mal.'))
            break;
          }
          case 401: {
            dispatch(getProfileErrorAction('No autorizado.'))
          }
          default:
            break;
        }
      })
      .catch((err) => {
        console.log('error', err)
        dispatch(getProfileErrorAction('Algo falla, asegurse que tiene conexión a internet.'))
      })
  }
}
