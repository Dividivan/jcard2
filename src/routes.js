import { StackNavigator, TabNavigator } from 'react-navigation'
import LoginPage from './containers/LoginPage'
import AccessPage from './containers/AccessPage'
import UsesPage from './containers/UsesPage'
import ProfilePage from './containers/ProfilePage'
import TokenPage from './containers/TokenPage'

const Navigator = StackNavigator({
  Login: {
    screen: LoginPage,
    navigationOptions: {
      header: null
    }
  },
  Home: {
    screen: TabNavigator({
      Accesos: {
        screen: AccessPage
      },
      Usos: {
        screen: UsesPage
      },
      QR: {
        screen: TokenPage
      },
      Perfil: {
        screen: ProfilePage
      }
    }, {
      tabBarPosition: 'top',
      swipeEnabled: true,
      tabBarOptions: {
        activeTintColor: 'yellow',
        inactiveTintColor: 'white',
        labelStyle: {
          fontSize: 10,
          paddingVertical: 5
        },
        style: {
          backgroundColor: '#2196f3',
        },
      }

    }),
    navigationOptions: {
      header: null
    }
  }
})

export default Navigator