// @flow
import { GET_ACCESS, GET_ACCESS_SUCCESS, GET_ACCESS_FAIL } from '../constants/actionsType'

const INITIAL_STATE = {
  loading: false,
  error: false,
  list: []
}

const access = (state: Object = INITIAL_STATE, action: Object): Object => {
  switch (action.type) {
    case GET_ACCESS:
      return {
        ...state,
        loading: true,
        error: false
      }
    case GET_ACCESS_SUCCESS: {
      return {
        loading: false,
        error: false,
        list: action.payload.access
      }
    }
    case GET_ACCESS_FAIL:
      return {
        loading: false,
        error: action.payload.error,
        list: []
    }
    default:
      return state
  }
}

export default access
