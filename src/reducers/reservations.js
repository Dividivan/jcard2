// @flow
import { GET_RESERVATIONS, GET_RESERVATIONS_SUCCESS, GET_RESERVATIONS_FAIL } from '../constants/actionsType'

const INITIAL_STATE = {
  loading: false,
  error: false,
  list: []
}

const reservations = (state: Object = INITIAL_STATE, action: Object): Object => {
  switch (action.type) {
    case GET_RESERVATIONS:
      return {
        ...state,
        loading: true,
        error: false
      }
    case GET_RESERVATIONS_SUCCESS: {
      return {
        loading: false,
        error: false,
        list: action.payload.reservations
      }
    }
    case GET_RESERVATIONS_FAIL:
      return {
        loading: false,
        error: action.payload.error,
        list: []
    }
    default:
      return state
  }
}

export default reservations
