// @flow
import { VALIDATE, VALIDATE_SUCCESS, VALIDATE_FAIL, LOGOUT } from '../constants/actionsType'
import { AsyncStorage } from "react-native"

const INITIAL_STATE = {
  loading: false,
  error: false,
  token: undefined,
  domain: undefined,
  isAuth: false
}

const login = (state: Object = INITIAL_STATE, action: Object): Object => {
  switch (action.type) {
    case VALIDATE:
      return {
        loading: true,
        error: undefined,
        token: undefined,
        domain: undefined,
        isAuth: false
      }
    case VALIDATE_SUCCESS:
      return {
        loading: false,
        error: false,
        token: action.payload.token,
        domain: action.payload.domain,
        isAuth: true
      }
    case VALIDATE_FAIL:
      return {
        loading: false,
        error: action.payload.error,
        token: undefined,
        domain: undefined,
        isAuth: false
      }
    case LOGOUT:
      return INITIAL_STATE
    default:
      return state
  }
}

export default login
