// @flow
import { GET_SERVICES, GET_SERVICES_SUCCESS, GET_SERVICES_FAIL } from '../constants/actionsType'

const INITIAL_STATE = {
  loading: false,
  error: false,
  list: []
}

const services = (state: Object = INITIAL_STATE, action: Object): Object => {
  switch (action.type) {
    case GET_SERVICES:
      return {
        ...state,
        loading: true,
        error: false
      }
    case GET_SERVICES_SUCCESS: {
      return {
        loading: false,
        error: false,
        list: action.payload.services
      }
    }
    case GET_SERVICES_FAIL:
      return {
        loading: false,
        error: action.payload.error,
        list: []
    }
    default:
      return state
  }
}

export default services
