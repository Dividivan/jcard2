import { combineReducers } from 'redux'
import login from './login'
import profile from './profile'
import qr from './qr'
import access from './access'
import services from './services'
import reservations from './reservations'

export default function getRootReducer(navReducer) {
  return combineReducers({
    nav: navReducer,
    login,
    profile,
    qr,
    access,
    services,
    reservations
  })
}
