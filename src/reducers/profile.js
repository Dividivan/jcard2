// @flow
import { GET_PROFILE, GET_PROFILE_SUCCESS, GET_PROFILE_FAIL } from '../constants/actionsType'

const defaultProfile = {
  fullname: "-",
  email: "-",
  document: "-",
  address: "-",
  city: "-",
  state: "-",
  zip: "-",
  country: "-",
  mobile: "-"
}

const INITIAL_STATE = {
  loading: false,
  error: false,
  profile: defaultProfile
}

const profile = (state: Object = INITIAL_STATE, action: Object): Object => {
  switch (action.type) {
    case GET_PROFILE:
      return {
        ...state,
        loading: true,
        error: false
      }
    case GET_PROFILE_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: false,
        profile: action.payload.profile
      }
    }
    case GET_PROFILE_FAIL:
      return {
        loading: false,
        error: action.payload.error,
        list: defaultProfile
    }
    default:
      return state
  }
}

export default profile
