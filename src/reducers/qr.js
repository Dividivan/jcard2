// @flow
import { GET_ID, GET_ID_SUCCESS, GET_ID_FAIL } from '../constants/actionsType'

const INITIAL_STATE = {
  loading: false,
  error: false,
  code: false
}

const qr = (state: Object = INITIAL_STATE, action: Object): Object => {
  switch (action.type) {
    case GET_ID:
      return {
        code: false,
        loading: true,
        error: false
      }
    case GET_ID_SUCCESS: {
      return {
        loading: false,
        error: false,
        code: action.payload.code
      }
    }
    case GET_ID_FAIL:
      return {
        loading: false,
        error: action.payload.error,
        code: false
      }
    default:
      return state
  }
}

export default qr
