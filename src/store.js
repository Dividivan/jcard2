import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import getRootReducer from './reducers'

const getStore = (navReducer, initialStore) => createStore(
  getRootReducer(navReducer),
  initialStore,
  applyMiddleware(thunk)
)

export default getStore
