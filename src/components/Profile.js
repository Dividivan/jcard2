import React, { Component } from 'react'
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity
} from 'react-native'

type Props = {
  profile: {},
  loading: boolean,
  error: string,
  qr: {},
  handleOnLoad: () => void,
  handleLoadQR: () => void,
  logout: () => void
}

const profileFields = {
  fullname: "Nombre:",
  email: "Email:",
  document: "Documento:",
  address: "Dirección:",
  city: "Ciudad:",
  state: "Estado:",
  zip: "C.P:",
  country: "País",
  mobile: "Móvil:"
}

export default class Profile extends Component {
  props: Props;

  constructor(props) {
    super(props);
    const { handleOnLoad } = props

    setTimeout(() => handleOnLoad(), 500)
  }

  _renderItem = ({item}) => (
    <View style={styles.row}>
      <Text style={styles.label}>
        {profileFields[item[0]]}
      </Text>
      <Text style={styles.mainText}>
        {item[1]}
      </Text>
    </View>
  )

  render() {
    const { loading, profile, logout } = this.props;

    return (
      <View>
        <FlatList
          data={Object.entries(profile)}
          refreshing={loading}
          keyExtractor={(item) => item[0]}
          renderItem={this._renderItem}
          ListFooterComponent={() => {
            return (
              <TouchableOpacity
                disabled={loading}
                onPress={() => logout()}
                activeOpacity={.5}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>Logout</Text>
                </View>
              </TouchableOpacity>
            )
          }}
        />
      </View>
    )
  }
}

var styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: 40,
    padding: 10,
    backgroundColor: 'white',
    marginBottom: 3
  },
  mainText: {
    flex: 1
  },
  label: {
    color: 'grey',
    flex: 1
  },
  button: {
    backgroundColor: "#2196f3",
    paddingVertical: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
  },
  buttonText: {
    color: "#FFF",
    fontSize: 18,
  }
});