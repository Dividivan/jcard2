import React, { Component } from 'react'
import moment from 'moment'
import {
  Text,
  View,
  FlatList,
  RefreshControl,
  StyleSheet
} from 'react-native'
import Separator from './Separator'

type Props = {
  list: [],
  loading: boolean,
  error: string,
  handleOnLoad: () => void
}

export default class Uses extends Component {
  props: Props;

  constructor(props) {
    super(props);
    const { handleOnLoad } = props

    setTimeout(() => handleOnLoad(), 500)
  }

  _renderHeader = ({item}) => (
    <View style={styles.header}>
      <Text style={styles.noMore}>
        Últimos 30 usos
      </Text>
    </View>
  )

  _renderItem = ({item}) => (
    <View style={styles.wrapper}>
      <View style={styles.row}>
        <Text style={styles.mainText}>
          {item.center}
        </Text>
        <Text style={styles.date}>
          {moment(item.timestamp*1000).format('D/MM/YYYY H:mm:ss')}
        </Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.mainText}>
          {item.service}
        </Text>
        <Text style={styles.date}>
          Precio: {item.price} € / Tarifa: {item.fare} €
        </Text>
      </View>
    </View>
  )

  _renderFooter = ({item}) => (
    <View style={styles.row}>
      <Text style={styles.noMore}>
        No hay más elementos
      </Text>
    </View>
  )

  render() {
    const { handleOnLoad, loading, list } = this.props;

    return (
      <FlatList
        data={list}
        refreshing={loading}
        keyExtractor={(item, index) => item.timestamp}
        renderItem={this._renderItem}
        ListHeaderComponent={this._renderHeader}
        ListFooterComponent={this._renderFooter}
        refreshControl={
          <RefreshControl
            refreshing={loading}
            onRefresh={() => handleOnLoad()}
          />
        }
      />
    )
  }
}

var styles = StyleSheet.create({
  wrapper: {
    marginBottom: 3
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: 20,
    padding: 10,
    backgroundColor: '#F6F6F6',
    marginBottom: 3
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: 20,
    padding: 10,
    backgroundColor: '#F6F6F6'
  },
  mainText: {
    flex: 1
  },
  date: {
    color: 'grey'
  },
  noMore: {
    textAlign: 'center',
    color: 'grey'
  }
})
