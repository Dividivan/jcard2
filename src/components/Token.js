import React, { Component } from 'react'
import {
  Platform,
  Text,
  View,
  FlatList,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity
} from 'react-native'

type Props = {
  qr: {},
  handleLoadQR: () => void
}

export default class Token extends Component {
  props: Props;

  constructor(props) {
    super(props);
    const { handleLoadQR } = props

    setTimeout(() => handleLoadQR(), 500)
  }

  render() {
    const { qr, handleLoadQR } = this.props;

    return (
      <View style={styles.column}>
        <View style={styles.qrWrapper}>
          {qr.loading &&
          <Text>
            Loading QR
          </Text>
          }
          {qr.code !== false &&
            <Image style={{width:300, height:300 }} source={{uri: `data:image/png;base64,${qr.code}`}} />
          }
        </View>
        <TouchableOpacity
          onPress={() => handleLoadQR()}
          activeOpacity={.5}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Refrescar</Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

var styles = StyleSheet.create({
  column: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingTop: 20
  },
  qrWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 40,
    paddingVertical: 30,
    height: 340
  },
  footer: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40
  },
  mainText: {
    flex: 1
  },
  label: {
    color: 'grey',
    flex: 1
  },
  button: {
    width: "100%",
    backgroundColor: "#2196f3",
    marginTop: 10,
    padding: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    color: "#FFF",
    fontSize: 18,
  }
});