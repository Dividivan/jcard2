import React from 'react'
import {
  View,
  StyleSheet
} from 'react-native'

function Separator() {
  return (
    <View style={styles.separator}/>
  )
}

export default Separator

var styles = StyleSheet.create({
  separator: {
    flex: 1,
    borderBottomWidth: 0.5,
    borderBottomColor: '#d6d7da',
  }
});