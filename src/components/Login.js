import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  KeyboardAvoidingView,
  Text,
  View,
  Image,
  Dimensions,
  TextInput,
  Button,
  TouchableOpacity
} from 'react-native';

const { width, height } = Dimensions.get("window");

type Props = {
  isAuth: boolean,
  loading: boolean,
  error: string,
  handleClick: () => void,
  navigateTo: () => void,
}

type State = {
  username: '',
  password: ''
}

export default class Login extends Component {
  props: Props;
  state: State;
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
  }

  componentWillMount() {
    const { isAuth, navigateTo } = this.props

    navigateTo(isAuth ? 'Home' : 'Login')
  }

  componentWillUpdate (nextProps: Props) {
    const { isAuth, navigateTo } = nextProps
    const { isAuth: wasAuth } = this.props


    if (!wasAuth && isAuth) {
      navigateTo('Home')
    }
  }

  checkDomain() {
    const { handleClick } = this.props
    const userSplit = this.state.username.split('@')

    //handleClick('demo.cloudjcard.com', 'Maria', 'xS7FjR&2')
    //handleClick('demo.cloudjcard.com', 'Pedro', 'UkH!6RtF')
    handleClick(userSplit[1], userSplit[0], this.state.password)
  }

  render(){
    const { loading, error } = this.props;

    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <View style={styles.markWrap}>
        </View>
        <View style={styles.wrapper}>
          {error &&
            <Text style={styles.error}>
              {error}
            </Text>
          }
          <View style={styles.inputWrap}>
            <TextInput
              placeholder="Usuario@dominio.com"
              placeholderTextColor="lightgrey"
              underlineColorAndroid="transparent"
              style={styles.input}
              returnKeyType="next"
              keyboardType="email-address"
              autoCapitalize="none"
              autoCorrect={false}
              onSubmitEditing={() => this.passwordInput.focus()}
              onChangeText={(text) => this.setState({username: text})}
              value={this.state.username}
              selectionColor="#2196f3"
            />
          </View>
          <View style={styles.inputWrap}>
            <TextInput
              placeholder="Contraseña"
              placeholderTextColor="lightgrey"
              underlineColorAndroid="transparent"
              style={styles.input}
              secureTextEntry
              returnKeyType="go"
              ref={(input) => this.passwordInput = input}
              onChangeText={(text) => this.setState({password: text})}
              value={this.state.password}
              selectionColor="#2196f3"
            />
          </View>
          <TouchableOpacity
            disabled={loading}
            onPress={() => this.checkDomain()}
            activeOpacity={.5}>
            <View style={styles.button}>
              <Text style={styles.buttonText}>{loading ? 'Loading...' : 'Entrar'}</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.footer}>
          <Image
            style={{ maxWidth: 350, height: 83 }}
            source={require('../img/FEDER.gif')}
          />
        </View>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "grey"
  },
  footer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40
  },
  markWrap: {
    flex: 1,
    paddingVertical: 30,
  },
  mark: {
    width: null,
    height: null,
    flex: 1,
  },
  background: {
    width,
    height,
  },
  wrapper: {
    paddingVertical: 30,
  },
  inputWrap: {
    flexDirection: "row",
    marginVertical: 10,
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: "#CCC"
  },
  input: {
    flex: 1,
    paddingHorizontal: 10,
    color: 'white'
  },
  button: {
    backgroundColor: "#2196f3",
    paddingVertical: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
  },
  buttonText: {
    color: "#FFF",
    fontSize: 18,
  },
  error: {
    backgroundColor: "red",
    color: "white",
    padding: 10
  }
});